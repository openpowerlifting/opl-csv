DATE := $(shell date --iso-8601)

.PHONY: all
all: zipfiles book

# The book cannot build into a shared public/ because it overwrites the target.
book:
	mdbook build

.PHONY: zipfiles
zipfiles: \
	files/openpowerlifting-latest.zip \
	files/openipf-latest.zip \
	files/opl-templates.zip

opl-data:
	git clone --depth 1 --branch main https://gitlab.com/openpowerlifting/opl-data.git

scratch/openpowerlifting.csv: opl-data
	mkdir -p files
	mkdir -p scratch/mdvars
	mkdir -p opl-data/build
	cd opl-data && cargo run --bin checker -- --compile-onefile
	mv opl-data/build/openpowerlifting.csv scratch/openpowerlifting.csv
	git -C opl-data rev-parse HEAD | tee files/COMMIT.txt scratch/mdvars/commit
	git -C opl-data rev-parse --short=8 HEAD > scratch/mdvars/commit-short
	git -C opl-data log -1 --pretty='%ad' --date=format:'%Y-%m-%d' HEAD > scratch/mdvars/authored-date

files/openpowerlifting-latest.zip: scratch/openpowerlifting.csv opl-data
	mkdir -p scratch/openpowerlifting-${DATE}
	cp scratch/openpowerlifting.csv scratch/openpowerlifting-${DATE}/openpowerlifting-${DATE}-$(shell cat scratch/mdvars/commit-short).csv
	LANG=en_US.UTF8 wc -l scratch/openpowerlifting-${DATE}/openpowerlifting-${DATE}-$(shell cat scratch/mdvars/commit-short).csv | cut -d " " -f 1 | xargs printf "%'u" > scratch/mdvars/openpowerlifting-linecount
	cp opl-data/LICENSE-DATA scratch/openpowerlifting-${DATE}/LICENSE.txt
	cp opl-data/docs/data-readme.md scratch/openpowerlifting-${DATE}/README.txt
	cd scratch && zip -r ../files/openpowerlifting-latest.zip openpowerlifting-${DATE}
	ls -sh ./files/openpowerlifting-latest.zip | cut -d " " -f 1 > scratch/mdvars/openpowerlifting-size

files/openipf-latest.zip: scratch/openpowerlifting.csv opl-data
	mkdir -p scratch/openipf-${DATE}
	head -n 1 scratch/openpowerlifting.csv > scratch/openipf-${DATE}/openipf-${DATE}-$(shell cat scratch/mdvars/commit-short).csv
	grep ",IPF," scratch/openpowerlifting.csv >> scratch/openipf-${DATE}/openipf-${DATE}-$(shell cat scratch/mdvars/commit-short).csv
	LANG=en_US.UTF8 wc -l scratch/openipf-${DATE}/openipf-${DATE}-$(shell cat scratch/mdvars/commit-short).csv | cut -d " " -f 1 | xargs printf "%'u" > scratch/mdvars/openipf-linecount
	cp opl-data/LICENSE-DATA scratch/openipf-${DATE}/LICENSE.txt
	cp opl-data/docs/data-readme.md scratch/openipf-${DATE}/README.txt
	cd scratch && zip -r ../files/openipf-latest.zip openipf-${DATE}
	ls -sh ./files/openipf-latest.zip | cut -d " " -f 1 > scratch/mdvars/openipf-size

files/opl-templates.zip: opl-data
	rm -rf scratch/opl-templates
	cp -r opl-data/docs/opl-templates scratch/opl-templates
	cd scratch && zip -r ../files/opl-templates.zip opl-templates

.PHONY: clean
clean:
	rm -rf book files opl-data public scratch
