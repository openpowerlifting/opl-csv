# Source Code

All OpenPowerlifting code is Free/Libre Software under the GNU AGPLv3+.

View all our projects here:
[https://gitlab.com/openpowerlifting](https://gitlab.com/openpowerlifting).
