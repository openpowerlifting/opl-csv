# Projects Using Our Data

The following is a partial list of projects, articles, and anything else using OpenPowerlifting data.

Did you use the data for something interesting?
[Contact us](https://www.openpowerlifting.org/contact) and we'll add your work to the list!

### Peer-Reviewed Publications
- **Machek, Steven B., et al.** (2020). [Myosin Heavy Chain Composition, Creatine Analogues, and the Relationship of Muscle Creatine Content and Fast-Twitch Proportion to Wilks Coefficient in Powerlifters.](https://journals.lww.com/nsca-jscr/Fulltext/2020/11000/Myosin_Heavy_Chain_Composition,_Creatine.2.aspx?__cf_chl_jschl_tk__=23a9385065579702420dfa0d225611c3e9fe8555-1604379318-0-AZRtKQuL4HpLIQALH1s5HR0ONnV8_KW3M2BWRoX8vEpeJV9JwLeFB6nuSai0TIfjxUBsjPIn4n8PdSJ8lUd368yg3v97XgX5cELMSA-2WkNwb8QSZlmLhwx81qoJ1b5Ithx08AIL6O-85APIFheDDB4-DqmmstCkuqsmsE9KbzRyFZQkfm_SLEkuaQQdygAlqYsNXVIAB-MBPGoya9uXx9qQiLB75OlPjRoSo_5nNLOgkSVD90PE29pS912eaplr3yrOz1jOyqdJiWTQagnbQjVKqO9Xh95VKCfbWmWCsKA8NTPDhaqUZOuHaDQj59D3dvs2Ip_PP0dx77zeUl_WZMVtPss_juGu2iY4NkjDuzkPqAnjSHPhtAAYfv5Aiy4j_zziM2Hh_MzRaNs0_7uCsz9vlS9QNyUoWGy5iP33gEJgZVv8CrK9q87ai00dh2nWfQ) *The Journal of Strength & Conditioning Research, 34*(11), 3022-3030.
	> Fast-twitch muscle fiber prevalence and muscle total creatine do not solely determine powerlifting skill variation.

- **Ferland, Pierre-Marc, et al.** (2020). [Efficiency of the Wilks and IPF Formulas at Comparing Maximal Strength Regardless of Bodyweight through Analysis of the Open Powerlifting Database.](https://digitalcommons.wku.edu/ijes/vol13/iss4/12/) *International Journal of Exercise Science, 13*(4), 567-582.
	> Compares the Wilks and IPF formulas, concluding that the design of Wilks is sound and it just needs updated constants.

- **Machek, Steven B., et al.** (2019). [Skeletal Muscle Fiber Type and Morphology in a Middle-Aged Elite Male Powerlifter Using Anabolic Steroids.](https://doi.org/10.1007/s42978-019-00039-z) *Journal of Science in Sport and Exercise, 1-8.*
	> Extends the known physiological limits of human muscle size and strength.

### Pre-Print Publications
- **Vigotsky, Andrew D., et al.** (2020). [Improbable data patterns in the work of Barbalho et al.](https://pure.solent.ac.uk/ws/files/17166774/Barbalho_White_Paper_7_.pdf) *SportRxiv.*
	> Uses the OpenPowerlifting dataset to illustrate statistical concepts used for the work in the white-paper.

### Academic Presentations

- **Steven Machek, MS, CSCS.** (2019). [Total Creatine and Creatine-Associated Markers in Relation to Wilks Coefficient.](https://www.researchgate.net/publication/333811414_Total_Creatine_and_Creatine-Associated_Markers_in_Relation_to_Wilks_Coefficient_in_Both_Male_and_Female_Powerlifters) *International Society of Sports Nutrition, 16th Annual Conference.*
	> Total muscle creatine content is not a significant predictor of powerlifting performance.

- **Marisa Brooks.** (2019). [Powerlifting in the USA.](https://youtu.be/8vr4i9Lb3Rg) *UIUC Library and Information Sciences.*
	> Investigates participation trends across USA regions from 2016 through 2018.

### Articles

- **Stronger By Science.** (2018). [How Sex, Strength, and Age Affect Strength Gains in Powerlifters.](https://www.strongerbyscience.com/predict-strength-gains/)
	> Factors assumed to predict rate of strength increase are less well-correlated than commonly believed.

- **Peidi Wu.** (2018). [A Better Wilks Formula.](https://peidiwu.com/a-better-wilks-formula/)
	> Proposes a new ranking formula based on Z-scores.

- **Elias Oziolor, Ph.D.** (2018). [Getting old? You can still lift!](https://oziolor.wordpress.com/2018/05/19/part-i-getting-old-you-can-still-lift/)
	> Examines the relationship between age and total, concluding that age is less of a factor than commonly believed — "detraining" with age is mostly due to bodyweight loss.

- **Stronger By Science.** (2017). [How to Get Strong: What is Strong?](https://www.strongerbyscience.com/how-to-get-strong-what-is-strong/)
	> Powerlifters, on the whole, don't seem to be meaningfully improving. However, the number of competitors has increased almost 5-fold, which accounts for the increases we've seen in world records and top-level competition. USAPL strength analytics.

- **Greg Nuckols.** (2017). [Group Data Don't Tell You Much About Individuals.](http://gregnuckols.com/2017/04/24/group-data-dont-tell-much-individuals/)
	> Very clear group trends in "strength gained per day" mask the tremendous variability between individuals.

### Projects

- **Christoph Liebender.** (2022). [PowerliftingSharp.](https://github.com/Sir-Photch/PowerliftingSharp)
	> Lightweight C# scraper for the OpenPowerlifting webserver.

- **Pablo Bandinopla.** (2022). [Weight for Reps: SBD World Rank.](https://weightxreps.net/sbd-stats)
	> Calculates percentiles for competition lifts.
